using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using api_comunicacao;
using MimeKit;
using Newtonsoft.Json;
using Xunit;
using System.IO;

namespace XUnitTestApi_comunicacao
{
    public class ApiTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        public ApiTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
        }

        [Fact]
        public async Task TestPostEmailAsync()
        {
            // Arrange
            var Url = "api/emailWithoutFile";
            var request = new
            {
                email = "rfpanizzon@gmail.com",
                assunto = "testando os testes",
                texto = "funciono"
            };

            //Act
            var postResponse = await Client.PostAsync(Url, ContentHelper.GetStringContent(request));
                
            // Assert
            Assert.Equal("OK", postResponse.EnsureSuccessStatusCode().StatusCode.ToString());
        }

        [Fact]
        public async Task TestPostWebexAsync()
        {
            // Arrange
            var Url = "api/messageWebexWithoutFile";
            var request = new
            {
                email = "rafael.panizzon@stara.com.br",
                texto = "funciono"
            };

            //Act
            var postResponse = await Client.PostAsync(Url, ContentHelper.GetStringContent(request));

            // Assert
            Assert.Equal("OK", postResponse.EnsureSuccessStatusCode().StatusCode.ToString());
        }

        [Fact]
        public async Task TestPostEmailComAnexoAsync()
        {
            // Arrange
            var Url = "api/sendEmail";

            var formData = new MultipartFormDataContent()
            {
                { new StringContent("rfpanizzon@gmail.com"), "email" },
                { new StringContent("teste testando"), "assunto" },
                { new StringContent("testo"), "texto" }
            };

            var document = File.ReadAllBytes(@"../../../QQ.png");
            formData.Add(new ByteArrayContent(document), "uploadedFile", "QQ.png");
            var request = new HttpRequestMessage(HttpMethod.Post, Url)
            {
                Content = formData
            };

            //Act
            var postResponse = await Client.PostAsync(Url, request.Content);

            // Assert
            Assert.Equal("OK", postResponse.EnsureSuccessStatusCode().StatusCode.ToString());
        }
        
        [Fact]
        public async Task TestPostWebexComAnexoAsync()
        {
            // Arrange
            var Url = "api/sendMessageWebex";
            var formData = new MultipartFormDataContent()
            {
                { new StringContent("rafael.panizzon@stara.com.br"), "email" },
                { new StringContent("teste testando webex file"), "texto" }
            };
            
            var file1 = new ByteArrayContent(File.ReadAllBytes(@"../../../QQ.png"));
            file1.Headers.Add("Content-Type", "image/png");
            formData.Add(file1, "files", "QQ.png");

            var request = new HttpRequestMessage(HttpMethod.Post, Url)
            {
                Content = formData
            };
            //Act
            var postResponse = await Client.PostAsync(Url, request.Content);

            // Assert
            Assert.Equal("OK", postResponse.EnsureSuccessStatusCode().StatusCode.ToString());
        }

    }
}