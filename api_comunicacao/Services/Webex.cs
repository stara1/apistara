using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using api_comunicacao.Entities;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace api_comunicacao.Services
{
    public interface IWebex
    {
        Task<System.Net.Http.HttpResponseMessage> MessageWebex(string email, string texto, IFormFile files);
        Task<System.Net.Http.HttpResponseMessage> MessageWebexWithoutFile(string email, string texto);
    }

    public class Webex : IWebex
    {
        private readonly SmtpSettings _smtpSettings;
        private readonly IWebHostEnvironment _env;

        public Webex(IOptions<SmtpSettings> smtpSettings, IWebHostEnvironment env)
        {
            _smtpSettings = smtpSettings.Value;
            _env = env;
        }

        public async Task<System.Net.Http.HttpResponseMessage> MessageWebex(string email, string texto, IFormFile file)
        {
            Console.WriteLine("file: " + file.FileName);

            HttpClient cliente = new HttpClient();
            
            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "MDZhY2NiMDQtNTcwZS00MWJlLWJmZGEtZjMwYmUxOTEwMDJhZTY5NGQ5MjgtYTAz_PF84_10ad53e8-6f11-418b-8fc2-34b49b776ec7");
            try
            {
                var savePath = Path.Combine(Directory.GetCurrentDirectory(), file.FileName);
                var fileStream = new FileStream(savePath, FileMode.Create);
                file.CopyTo(fileStream);
                fileStream.Dispose();
                
                var formData = new MultipartFormDataContent()
                {
                    { new StringContent(email), "toPersonEmail" },
                    { new StringContent(texto), "text" }
                };

                var file1 = new ByteArrayContent(File.ReadAllBytes(savePath));
                file1.Headers.Add("Content-Type", file.ContentType);
                formData.Add(file1, "files", file.FileName);

                var response = await cliente.PostAsync("https://webexapis.com/v1/messages", formData);
                Console.WriteLine(response);

                File.Delete(savePath);

                return response;
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }

        public async Task<System.Net.Http.HttpResponseMessage> MessageWebexWithoutFile(string email, string texto)
        {
            HttpClient cliente = new HttpClient();

            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "MDZhY2NiMDQtNTcwZS00MWJlLWJmZGEtZjMwYmUxOTEwMDJhZTY5NGQ5MjgtYTAz_PF84_10ad53e8-6f11-418b-8fc2-34b49b776ec7");
            cliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                var values = new Dictionary<string, string>
                {
                    { "toPersonEmail", email },
                    { "text", texto }
                };

                var content = new FormUrlEncodedContent(values);
                var response = await cliente.PostAsync("https://webexapis.com/v1/messages", content);
                Console.WriteLine(response.ToString());
                return response;
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }
    }
}