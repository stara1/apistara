using System.ComponentModel.DataAnnotations;

namespace api_comunicacao.Entities
{
    public class BodyWebexWithoutFile
    {
        [Required]
        public string email { get; set; }

        [Required]
        public string texto { get; set; }
    }
}