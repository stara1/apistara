using System.ComponentModel.DataAnnotations;


namespace api_comunicacao.Entities
{
    public class BodyEmailWithoutFile
    {
        [Required]
        public string email { get; set; }

        [Required]
        public string assunto { get; set; }

        [Required]
        public string texto { get; set; }
    }
}