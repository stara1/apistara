
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace api_comunicacao.Entities
{
    public class BodyEmail
    {
        [Required]
        public string email { get; set; }

        [Required]
        public string assunto { get; set; }

        [Required]
        public string texto { get; set; }

        [Required]
        public IFormFile uploadedFile {get; set;}

    }
}