using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace api_comunicacao.Entities
{
    public class BodyWebex
    {
        [Required]
        public string email { get; set; }

        [Required]
        public string texto { get; set; }
        
        [Required]
        public IFormFile files {get; set;}
    }
}