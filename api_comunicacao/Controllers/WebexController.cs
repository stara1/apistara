using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api_comunicacao.Services;
using System.Net;
using api_comunicacao.Entities;

namespace api_comunicacao.Controllers
{
    [ApiController]
    [Route("api")]
    public class WebexController : ControllerBase
    {
        
        private readonly IWebex _webex;

        public WebexController(IWebex webex)
        {
            _webex = webex;
        }

        /// <summary>
        /// Envie uma mensagem de texto através do Webex.
        /// </summary>
        /// <response code="400">Campo incorreto</response>
        [HttpPost]
        [Route("messageWebexWithoutFile")]
        public async Task<IActionResult> sendMessageWithoutFile([FromBody] BodyWebexWithoutFile bodyWebexWithoutFile)
        {

            var response = await _webex.MessageWebexWithoutFile(bodyWebexWithoutFile.email, bodyWebexWithoutFile.texto);

            if (response.StatusCode == HttpStatusCode.OK) return Ok("Funcionou webex");
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Envie uma mensagem com anexo através do Webex.
        /// </summary>
        /// <response code="400">Campo incorreto</response>
        [HttpPost]
        [Route("sendMessageWebex")]
        public async Task<IActionResult> sendMessage([FromForm]BodyWebex bodyWebex)
        {
            Console.WriteLine("toPersonEmail: " + bodyWebex.email);
            Console.WriteLine("text: "+bodyWebex.texto);
            Console.WriteLine(bodyWebex.files);
            
            var response = await _webex.MessageWebex(bodyWebex.email, bodyWebex.texto, bodyWebex.files);
            if (response.StatusCode == HttpStatusCode.OK) {
                
                return Ok("Funcionou webex com anexo");
            }
            else if(response.StatusCode == HttpStatusCode.Conflict){
                return Conflict("Conflito");
            }
            else
            {
                return BadRequest();
            }
        }

    }
}