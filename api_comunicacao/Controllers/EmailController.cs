﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api_comunicacao.Services;
using api_comunicacao.Entities;


namespace api_comunicacao.Controllers
{
    [ApiController]
    [Route("api")]
    public class EmailController : ControllerBase
    {
        private readonly IEmail _email;

        public EmailController(IEmail email)
        {
            _email = email;
        }

        /// <summary>
        /// Envie uma mensagem de texto para email.
        /// </summary>
        /// <response code="400">Campo incorreto</response>   
        [HttpPost]
        [Route("emailWithoutFile")]
        public async Task<IActionResult> sendEmailWithoutFile([FromBody] BodyEmailWithoutFile bodyEmailWithoutFile)
        {
            await _email.SendEmailWithoutFileAsync(bodyEmailWithoutFile.email, bodyEmailWithoutFile.assunto, bodyEmailWithoutFile.texto);
            
            return Ok("Realmente funcionou");
        }

        /// <summary>
        /// Envie uma mensagem com anexo através do Email.
        /// </summary>
        /// <response code="400">Campo incorreto</response>
        /// <response code="409">Conflito</response>
        [HttpPost]
        [Route("sendEmail")]
        public async Task<IActionResult> sendEmail([FromForm]BodyEmail bodyEmail)
        {

            await _email.SendEmailAsync(bodyEmail.email, bodyEmail.assunto, bodyEmail.texto, bodyEmail.uploadedFile);
            
            return Ok("Funcionou email com anexo");
        }
    }
}